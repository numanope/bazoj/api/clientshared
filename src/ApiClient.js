const sampleHeaders = [
	['Content-Type', 'application/json'],
];

function ApiClient(isOnline, fetch) {
	this.isOnline = isOnline;
	this.fetch = fetch;
}

ApiClient.prototype.set = function (type, method, url, headers, body) {
	this.setType(type);
	this.setMethod(method);
	this.setUrl(url);
	this.setHeaders(headers || null);
	this.setBody(body || null);
};

ApiClient.prototype.setType = function (type) {
	this.type = type?.toLowerCase();
};

ApiClient.prototype.setMethod = function (method) {
	this.method = method;
};

ApiClient.prototype.setUrl = function (url) {
	this.url = url;
};

ApiClient.prototype.setHeaders = function (headers) {
	if (headers != null && Array.isArray(headers)) {
		this.headers = headers;
	}
	else {
		this.headers = sampleHeaders;
	}
};

ApiClient.prototype.setBody = function (body) {
	if (body != null && typeof body === 'object') {
		this.body = body;
	}
	else {

		this.body = null;
	}
};

ApiClient.prototype.query = async function () {
	if (!(await this.isOnline())) {
		throw new Error('Offline');
	}
	else {
		let options = {
			method: this.method,
			headers: this.headers,
			body: this.body,
		};
		try {
			options = prepareOptions(options);
			const url = checkUrl(this.url);
			let data = await this.fetch(url, options);
			data = await data.json();
			return data;
		}
		catch (err) { if (err) throw err; }
	}
};

const prepareOptions = function (options) {
	let output = JSON.parse(JSON.stringify(options));

	if (output.method === 'GET'
			|| output.method === 'HEAD'
			|| output.body == null) {
		delete output.body;
	}
	else if (output.body != null && typeof output.body === 'object') {
		output.body = JSON.stringify(output.body);
	}
	else if (typeof output.body !== 'string') {
		throw new Error('Cannot prepare body');
	}

	return output;
};

const checkUrl = function (url) {
	if (url == null || url === '') {
		throw new Error('Invalid url');
	}
	return url;
};

export default ApiClient;
